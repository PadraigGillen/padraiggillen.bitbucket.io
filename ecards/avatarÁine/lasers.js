const colors = ["blue", "red", "purple", "green"];
const BEEM_COUNT = 35;

airhorn = new Audio("airhorn.mp3");
trap_beat = new Audio("club.mp3");
trap_beat.loop = true;
city_beat = new Audio("city_lights.mp3");
city_beat.loop = true;
var club_beat_playing = true;

function load_lasers() {
  // hide the start button and crank the beats
  document.getElementById("gobutt").style.display = "none";
  trap_beat.play();
  airhorn.play();

  // generate all the laser beams
  for (var i=0; i<BEEM_COUNT; i++) {
    var beem = document.createElement("div");
    beem.className = 'laser-beam ';
    beem.className += colors[Math.floor(Math.random()*4)];
    var anim_del = Math.random()*-3;
    beem.style.setProperty('animation-delay', anim_del.toFixed(4) +'s');
    document.body.appendChild(beem);
  }
  // bring my message out after 5 seconds
  setTimeout(function(){ document.getElementById("main").style.display = "block" }, 5000);
}

function toggle_music() {
  if (club_beat_playing) {
    trap_beat.pause();
    city_beat.play();
    club_beat_playing = false;
    document.getElementById("musicbutt").innerHTML = "Switch to party music";
  } else {
    city_beat.pause();
    trap_beat.play();
    club_beat_playing = true;
    document.getElementById("musicbutt").innerHTML = "Switch to classier music";
  }
}
