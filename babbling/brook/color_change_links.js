// please excuse me here
// this is a jumping off point

const colors = ["MediumPurple"
               ,"DarkGreen"
               ,"LightGreen"
               ,"LightPink"
               ];
const class2change = "cc_words";

const slowdown_factor = 1.1;
var timeout = 100; // milliseconds

// bump the color of each member of .class2change
function change_colors(color_idx = 0) {
	const new_color = colors[color_idx];
	var elems = Array.from(document.getElementsByClassName(class2change));
	elems.forEach( elem => { elem.style.color = new_color; });

	// an exponential decay while loop. train never stops, just slows down
	timeout = Math.round(timeout * slowdown_factor);
	color_idx = (color_idx + 1) % colors.length;
	setTimeout(change_colors, timeout, color_idx);
}
