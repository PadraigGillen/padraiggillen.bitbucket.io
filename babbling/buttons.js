// https://www.w3schools.com/howto/howto_css_fullscreen_video.asp
var toggle_btn    = document.getElementById("sound_toggle");
var influence_btn = document.getElementById("influence");
var escape_btn    = document.getElementById("escape");
var vid           = document.getElementById("squad_vid");

sound_playing = false;
sound_toggled = false;
speed_changed = false;

function toggle_sound() {
	if (!sound_toggled) {
		// show our 2nd button
		sound_toggled = true;
		influence_btn.style.display = "inline-block";
		vid.play()
	}
	sound_playing = !sound_playing;
	if (sound_playing) {
		toggle_btn.innerHTML = "Silence me";
	} else {
		toggle_btn.innerHTML = "Hear me";
	}
	vid.muted = !sound_playing;
}

function change_speed() {
	if (!speed_changed) {
		// show our 3rd button
		speed_changed = true;
		escape_btn.style.display = "inline-block";
		influence_btn.innerHTML = "Influence me more";
	}
	vid.playbackRate = Math.random() * (1.5) + 0.5;
}

function sleep(ms) {return new Promise(resolve => setTimeout(resolve, ms));}

async function lets_escape() {
	document.getElementById("content").innerHTML = "<h1>Reentering the stream...</h1>";

	vid.style.opacity = 1;
	// 2 second fade out
	while (vid.style.opacity > 0) {
		vid.style.opacity -= 0.01;
		await sleep(20);
	}
	window.location = "./brook";
}

// fix blank page when someone clicks the back button to get here
// https://stackoverflow.com/a/43043658
window.addEventListener("pageshow", function (event) {
	if ((typeof window.performance != "undefined" && window.performance.navigation.type === 2) ||
		event.persisted) {
		window.location.reload();
	}
});
